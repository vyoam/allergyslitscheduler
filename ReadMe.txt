ALLERGY SLIT SCHEDULER
Nov 2018
Akash Amat, akash.amat@gmail.com
Bengaluru, India

INSTRUCTIONS FOR USE:
1. The original zip file for this scheduler is available at https://bitbucket.org/vyoam/allergyslitscheduler/downloads/ - 'Download repository' option.
2. Unzip it and go inside the folder. You'll find "AllergySLITScheduler.html"
3. Open it in Chrome browser.
4. Select a start date, choose between build-up or maintenace phase, and then it'll fill out the tables.
5. You can print from there itself, OR you can select the contents (Ctrl+A), copy (Ctrl+C) and paste (Ctrl+V) in Word document.